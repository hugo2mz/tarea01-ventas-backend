package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Venta;

public interface IVentaService {

	Venta registrar(Venta venta);
	Venta listarId(Integer idVenta);
	List<Venta> listar();
	
}
