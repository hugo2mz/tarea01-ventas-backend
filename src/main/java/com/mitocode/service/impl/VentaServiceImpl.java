package com.mitocode.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IVentaDAO;
import com.mitocode.model.Venta;
import com.mitocode.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaDAO dao;

	@Override
	public Venta registrar(Venta venta) {
		venta.getDetalleVenta().forEach(x -> x.setVenta(venta));
		return dao.save(venta);
	}

	@Override
	public Venta listarId(Integer idVenta) {
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}
	
}
