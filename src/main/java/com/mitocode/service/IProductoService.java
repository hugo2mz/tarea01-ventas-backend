package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Producto;

public interface IProductoService {

	Producto registrar(Producto producto);
	void modificar(Producto producto);
	void eliminar(Integer idProducto);
	Producto listarId(Integer idProducto);
	List<Producto> listar();
	
}
